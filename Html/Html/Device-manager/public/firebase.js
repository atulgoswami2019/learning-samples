var firebaseConfig = {
    apiKey: "AIzaSyCfbn1QuJ-28C6Z1fktvNEXkkM08_xDLI0",
    authDomain: "device-manager-90eee.firebaseapp.com",
    databaseURL: "https://device-manager-90eee.firebaseio.com",
    projectId: "device-manager-90eee",
    storageBucket: "device-manager-90eee.appspot.com",
    messagingSenderId: "120265790431",
    appId: "1:120265790431:web:93fba3500975c56248aa6a",
    measurementId: "G-TF0X9XL622"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();

  var storage = firebase.storage();
  var storageRef = storage.ref();
  
  $('#dev1').find('#img').html('');
  
  var i=0;
  
  storageRef.child('Device Images/').listAll().then(function(result){
  
      result.items.forEach(function(imageRef){
  
          //console.log("Image reference" + imageRef.toString()); 
  
          i++;
          displayImage1(i,imageRef)
      });
  });
  
  
  function displayImage1(row, images){
  
      images.getDownloadURL().then(function(url){
  
        console.log(url);
  
        let new_html = '';

        new_html += '<div style="width:100px; height:120px; float:left;">';

        new_html += '<img src="'+url+'" width="100px" height="100px" style="float:left">';

        new_html += '<button style="width:100px; height:20px;"><a href="'+url+'">Select</a></button>';

        new_html += '</div>';
          
  
        $('#dev1').find('#img').append(new_html); 
    });
  }



  $('#dev2').find('#img').html('');
  
  var i=0;
  
  storageRef.child('Device 2/').listAll().then(function(result){
  
      result.items.forEach(function(imageRef){
  
          //console.log("Image reference" + imageRef.toString()); 
  
          i++;
          displayImage2(i,imageRef)
      });
  });
  
  
  function displayImage2(row, images){
  
      images.getDownloadURL().then(function(url){
  
        console.log(url);
  
        let new_html = '';

        new_html += '<div style="width:100px; height:120px; float:left;">';

        new_html += '<img src="'+url+'" width="100px" height="100px" style="float:left">';

        new_html += '<button style="width:100px; height:20px;"><a href="'+url+'">Select</a></button>';

        new_html += '</div>';
          
  
        $('#dev2').find('#img').append(new_html); 
    });
  }



  $('#dev1').find('#img').html('');
  
  var i=0;
  
  storageRef.child('Device 3/').listAll().then(function(result){
  
      result.items.forEach(function(imageRef){
  
          //console.log("Image reference" + imageRef.toString()); 
  
          i++;
          displayImage3(i,imageRef)
      });
  });
  
  
  function displayImage3(row, images){
  
      images.getDownloadURL().then(function(url){
  
        console.log(url);
  
        let new_html = '';

        new_html += '<div style="width:100px; height:120px; float:left;">';

        new_html += '<img src="'+url+'" width="100px" height="100px" style="float:left">';

        new_html += '<button style="width:100px; height:20px; onclick="changeBodyImage('+url+')">Select</button>';

        new_html += '</div>';
          
  
        $('#dev3').find('#img').append(new_html); 
    });

    
  }

 function changeBodyImage(url){
    document.getElementById("bg").style.backgroundImage = "url(url)";
  }



  
            function fun(evt, devname) {
            var i, devcontent, devlinks;
            devcontent = document.getElementsByClassName("devcontent");
            for (i = 0; i < devcontent.length; i++) {
                devcontent[i].style.display = "none";
            }
            devlinks = document.getElementsByClassName("devlinks");
            for (i = 0; i < devlinks.length; i++) {
                devlinks[i].className = devlinks[i].className.replace(" active", "");
            }
            document.getElementById(devname).style.display = "block";
            evt.currentTarget.className += " active";


            }

            document.getElementById("active").click();
        