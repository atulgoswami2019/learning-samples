var firebaseConfig = {
    apiKey: "AIzaSyCfbn1QuJ-28C6Z1fktvNEXkkM08_xDLI0",
    authDomain: "device-manager-90eee.firebaseapp.com",
    databaseURL: "https://device-manager-90eee.firebaseio.com",
    projectId: "device-manager-90eee",
    storageBucket: "device-manager-90eee.appspot.com",
    messagingSenderId: "120265790431",
    appId: "1:120265790431:web:93fba3500975c56248aa6a",
    measurementId: "G-TF0X9XL622"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

var uploader=document.getElementById('uploader');
var filebutton=document.getElementById('filebutton');

filebutton.addEventListener('change', function(e){

    var file = e.target.files[0];

    var storageRef = firebase.storage().ref('Device 1/' + file.name);

    var task = storageRef.put(file);

    task.on('state_changed',
    
        function progress(snapshot){
            var percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            uploader.value = percentage;
        },

        function error(err){

        },

        function complete(){
            task.snapshot.ref.getDownloadURL().then(function(downloadURL) {
            //console.log('File available at', downloadURL);
            addImagePath(downloadURL);
            });
        }

    );
});








function fun(evt, devname) {
    var i, devcontent, devlinks;
    devcontent = document.getElementsByClassName("devcontent");
    for (i = 0; i < devcontent.length; i++) {
        devcontent[i].style.display = "none";
    }
    devlinks = document.getElementsByClassName("devlinks");
    for (i = 0; i < devlinks.length; i++) {
        devlinks[i].className = devlinks[i].className.replace(" active", "");
    }
    document.getElementById(devname).style.display = "block";
    evt.currentTarget.className += " active";


    }

    document.getElementById("active").click();