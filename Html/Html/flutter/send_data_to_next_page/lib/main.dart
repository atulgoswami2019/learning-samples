import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "List View",
    home: Scaffold(
      appBar: AppBar(
        title: Text("Long List"),
      ),
      body: getListView(),
    ),
  ));
}

List<String> getListElemets() {
  var items = List<String>.generate(20, (counter) => "Item $counter");
  return items;
}

Widget getListView() {

  var listItems = getListElemets();
  var listView = ListView.builder(
    itemBuilder: (context, index) {
      return ListTile(
        title: Text(listItems[index]),
        onTap: (){
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ItemDetails()),
          );
        },
      );

    }
    );
    return listView;
}

class ItemDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Details"),
      ),
      body: Center(
        child: Text("Hello"),
      ),
      
    );
  }
}