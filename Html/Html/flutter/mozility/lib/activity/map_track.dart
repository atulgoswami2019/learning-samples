import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:getflutter/getflutter.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:mozility/activity/DeviceStates.dart';
import 'package:mozility/activity/emp_Details.dart';
import 'package:mozility/json/Location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mozility/json/Employee.dart';
import 'package:intl/intl.dart';
import 'package:ff_navigation_bar/ff_navigation_bar.dart';
import 'package:mozility/json/Sevices.dart';



Future<List<Employee>> fetchEmployee() async{
  var jsonData;
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String token = sharedPreferences.getString('token');
  String id = sharedPreferences.getString('adminEID');
  final response = await http.get('https://ap1.mozility.com/api/common/subordinates?empId=$id&\$filter=UserType%20ne%20%27web%27',
  headers:{'Content-Type' : 'application/json', 'Authorization' : 'Bearer $token'}
  );
  if(response.statusCode == 200){
    return employeeFromJson(response.body); 
  }
  else{
    jsonData = json.decode(response.body);
    throw Exception(jsonData['message']);
  }
}




class MapTrack extends StatefulWidget {
  @override
  _MapTrackState createState() => _MapTrackState();
}

class _MapTrackState extends State<MapTrack> {

  

  static Set<Marker> markers = Set();

  List data;

  Future drivLocation() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    print(sharedPreferences.getString('r_token'));
    String _date = new DateFormat("dd-MMMM-yy").format(DateTime.now());
    String token = sharedPreferences.getString('token');
    String id = sharedPreferences.getString('adminEID');
    final response = await http.get('https://ap1.mozility.com/api/common/DriversCurrentLocation?Id=$id&fromDate=$_date&toDate=$_date',
    headers: <String, String>{'Authorization' : 'Bearer $token'}
    );
    if(response.statusCode == 200){
      data = json.decode(response.body);
      for(var i=0; i<data.length; i++){
        //debugPrint("${data[i]["lat"]}");
        markers.add(
          Marker(
            markerId: MarkerId("${data[i]["\u0024id"]}"),
            infoWindow: InfoWindow(
              title: "${data[i]["name"]}",
            ),
            icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
            position:  LatLng(data[i]["lat"], data[i]["long"]),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => EmpDetails(id: data[i]["empId"], name: data[i]["name"], mobile: data[i]["contactNo"], date: _date,)));
            },
          )
        );
      }
    }
    else{
      throw Exception('Failed to load API');
    }
    
  }

  static Completer<GoogleMapController> mapController = Completer();

  static LatLng _center = const LatLng(22.062083, 78.938766);

  @override
  void initState() {
    super.initState();
    drivLocation();
  }

 
  //static List<Marker> allMarkers = [];

  // List<Marker> addMarkers = [
  //   Marker(
  //     markerId: MarkerId("Quark city Id"),
  //     infoWindow: InfoWindow(
  //       title: "Quark city",
  //     ),
  //     icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
  //     position:  LatLng(30.706129, 76.692422)
  //   ),
  //   Marker(
  //     markerId: MarkerId("Godrej Company Id"),
  //     infoWindow: InfoWindow(
  //       title: "Godrej Company",
  //     ),
  //     icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
  //     position:  LatLng(30.701916, 76.695063)
  //   ),
  //   Marker(
  //     markerId: MarkerId("Radha Swami Chouwk Id"),
  //     infoWindow: InfoWindow(
  //       title: "Radha Swami Chouwk",
  //     ),
  //     icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
  //     position:  LatLng(30.703092, 76.701069)
  //   ),
  // ];

 

  int currentTabIndex = 0;

  final List<Widget> _pages = [
    Emp(),
    Scaffold(
      body: GoogleMap(
        onMapCreated: (GoogleMapController controller){
          mapController.complete(controller);
        },
        markers: markers,
        initialCameraPosition: CameraPosition(
          target: _center,
          zoom: 4.0,
        ),
      ),
    ),
  ];

  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Map Tracker'),
      ),

      backgroundColor: Colors.white70,

      body: _pages[currentTabIndex],

     
      bottomNavigationBar: FFNavigationBar(
        theme: FFNavigationBarTheme(
          barBackgroundColor: Colors.white,
          selectedItemBorderColor: Colors.yellow,
          selectedItemBackgroundColor: Colors.green,
          selectedItemIconColor: Colors.white,
          selectedItemLabelColor: Colors.black,
        ),
        selectedIndex: currentTabIndex,
        onSelectTab: (index) {
          setState(() =>  currentTabIndex = index);        
        },
         items:[
         FFNavigationBarItem(
           iconData: FontAwesome.user,
           label: 'Employee',
         ),
         FFNavigationBarItem(
           iconData: Icons.map,
           label: 'Map',
         ),
       ],
      ),
    );
  }
}

class Emp extends StatefulWidget {
  @override
  _EmpState createState() => _EmpState();
}

class _EmpState extends State<Emp> {

  List<Employee> emp = List();
  List<Elist> filteredemp = List();
  List<Elist> bfilteredemp = List();
  List<Location> lEmp = List();
  List<Location> lfilteredEmp = List();
  List<Location> lbfilteredEmp = List();
  List<Location> liveEmp = List();
  List<Location> tempLiveEmp = List();
  List<Location> offlineEmp = List();
  List<Location> tempOfflineEmp = List();
  List<Location> gpsOffEmp = List();
  List<Location> tempEndEmp = List();
  List<Location> endEmp = List();
  List<Location> tempStartEmp = List();
  List<Location> startEmp = List();
  List<Location> tempOldStartEmp = List();
  List<Location> oldStartEmp = List();
  List<Location> territoryEmp = List();
  List<Location> tempIdleEmp = List();
  List<Location> idleEmp = List();
  List<Elist> tempnewl = List();
  List<Elist> newl = List();
  var off = 0;
  var live = 0;
  var gps = 0;
  var total = 0;
  var old = 0;
  var start = 0;
  var territory = 0;
  var idle = 0;
  var end = 0;
  Color col = Colors.grey;
  Color totalbg = Colors.yellow;
  Color livebg = Colors.grey[50];
  Color offlinebg = Colors.grey[50];
  Color gpsbg = Colors.grey[50];
  Color startbg = Colors.grey[50];
  Color territorybg = Colors.grey[50];
  Color oldbg = Colors.grey[50];
  Color idlebg = Colors.grey[50];
  Color endbg = Colors.grey[50];

  countbtn(){
    var d2 = DateTime.now();
    var d1,d3,d5,d6,value,valuecheck;
    String d4;
    var otherinfo;
    for(int i=0; i<lEmp.length; i++){
      d1 = lEmp[i].timeStamp;
      d3 = (((d2.difference(d1).inMilliseconds) / 1000) / 60).toString();
      value = d3.split(".");
      valuecheck = int.parse(value[0]);
      otherinfo = json.decode(lEmp[i].otherInfo);
      if(otherinfo["DayStatus"] == "1"){
        if(valuecheck<30){
          tempLiveEmp.add(lEmp[i]);
          if(lEmp[i].userActivity == "still"){
            var tilltime = 0;
            tilltime = int.parse(otherinfo["ActivityStatusSince"]);
            if(tilltime > 30){
              tempIdleEmp.add(lEmp[i]);
            }
          }
        } else{
          tempOfflineEmp.add(lEmp[i]);
        }
        
        d4 = otherinfo["DayStatusTime"];
        d5 = (d2.toString()).substring(0, 10);
        d6 = d4.substring(0, 10);
        if(d5 == d6){
          tempStartEmp.add(lEmp[i]);
        } else{
          tempOldStartEmp.add(lEmp[i]);
        }
      } else{
        tempEndEmp.add(lEmp[i]);
      }
    }
    setState(() {
        liveEmp = tempLiveEmp;
        offlineEmp = tempOfflineEmp;
        gpsOffEmp = lEmp.where((e) =>
          e.gpsStatus != 'ON'
        ).toList();
        startEmp = tempStartEmp;
        oldStartEmp = tempOldStartEmp;
        territoryEmp = lEmp.where((e) =>
          e.interritory == 0
        ).toList();
        idleEmp = tempIdleEmp;
        endEmp = tempEndEmp;
        live = liveEmp.length;
        off = offlineEmp.length;
        gps = gpsOffEmp.length;
        start = startEmp.length;
        territory = territoryEmp.length;
        old = oldStartEmp.length;
        idle = idleEmp.length;
        end = endEmp.length;
        newl = tempnewl;
      });
  }

  createList(){
    for(int i=0; i<emp.length; i++){
      String id;
      String name;
      String contactNo;
      String managerName;
      int level;
      String stastart = "";
      String stalive = "";
      String staoff = "";
      String stagpsoff = "";
      String staterritory = "";
      String staold = "";
      String staidle = "";
      String staend = "";
      for(int j=0; j<startEmp.length; j++){
        if(emp[i].empId == startEmp[j].empId){
          stastart = "Start ";
        }
      }
      for(int j=0; j<liveEmp.length; j++){
        if(emp[i].empId == liveEmp[j].empId){
          stalive = "Live ";
        }
      }
      for(int j=0; j<offlineEmp.length; j++){
        if(emp[i].empId == offlineEmp[j].empId){
          staoff = "Offline ";
        }
      }
      for(int j=0; j<gpsOffEmp.length; j++){
        if(emp[i].empId == gpsOffEmp[j].empId){
          stagpsoff = "GPS-Offline ";
        }
      }
      for(int j=0; j<territoryEmp.length; j++){
        if(emp[i].empId == territoryEmp[j].empId){
          staterritory = "Territory ";
        }
      }
      for(int j=0; j<oldStartEmp.length; j++){
        if(emp[i].empId == oldStartEmp[j].empId){
          staold = "Old-Start ";
        }
      }
      for(int j=0; j<idleEmp.length; j++){
        if(emp[i].empId == idleEmp[j].empId){
          staidle = "Idle ";
        }
      }
      for(int j=0; j<endEmp.length; j++){
        if(emp[i].empId == endEmp[j].empId){
          staend = "End";
        }
      }
      id = emp[i].empId;
      name = emp[i].empName;
      contactNo = emp[i].empContactNo;
      managerName = emp[i].managerName;
      level = emp[i].empLevel;
      Elist li = new Elist(id: id, name: name,contactNo: contactNo, managerName: managerName, level: level, stastart: stastart, stalive: stalive, staoff: staoff, stagpsoff: stagpsoff, staterritory: staterritory, staold: staold, staidle: staidle, staend: staend);
      tempnewl.add(li);
    }

    setState(() {
      newl = tempnewl;
      bfilteredemp = newl;
      filteredemp = newl;
    });
  }

  @override
  void initState() {
    super.initState();
    fetchEmployee().then((dataFromServer){
      setState(() {
        emp = dataFromServer;
        total = emp.length;
      });
      Services.drivLocationEmp().then((data){
      setState(() {
        lEmp = data;
      });
      countbtn();
      createList();
    });
    });

    
  }

  static String _date = new DateFormat("dd-MMMM-yy").format(DateTime.now());


  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverFixedExtentList(
          itemExtent: 50.0,
          delegate: SliverChildListDelegate([
            ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: ActionChip(
                    avatar: CircleAvatar(
                      backgroundColor: Colors.purple,
                      child: Text("$total"),
                    ),
                    label: Text("Total", style: TextStyle(fontSize: 22),),
                    backgroundColor: totalbg,
                    shape: BeveledRectangleBorder(),
                    onPressed: (){
                      setState(() {
                        totalbg = Colors.yellow;
                        startbg = Colors.grey[50];
                        livebg = Colors.grey[50];
                        offlinebg = Colors.grey[50];
                        gpsbg = Colors.grey[50];
                        territorybg = Colors.grey[50];
                        oldbg = Colors.grey[50];
                        idlebg = Colors.grey[50];
                        endbg = Colors.grey[50];
                        col = Colors.grey;
                        bfilteredemp = newl;
                        filteredemp = bfilteredemp;
                      });
                    },
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: ActionChip(
                    avatar: CircleAvatar(
                      backgroundColor: Colors.red,
                      child: Text("$start"),
                    ),
                    label: Text("Start", style: TextStyle(fontSize: 22),),
                    backgroundColor: startbg,
                    shape: BeveledRectangleBorder(),
                    onPressed: (){
                      bfilteredemp = newl.where((element) => 
                      element.stastart == "Start "
                      ).toList();
                      // for(int i=0; i<start; i++){
                      //   for(int j=0; j<emp.length; j++){
                      //     if(emp[j].empId == startEmp[i].empId){
                      //       bfilteredemp.add(emp[j]);
                      //     }
                      //   }
                      // }
                      setState(() {
                        totalbg = Colors.grey[50];
                        startbg = Colors.yellow;
                        livebg = Colors.grey[50];
                        offlinebg = Colors.grey[50];
                        gpsbg = Colors.grey[50];
                        territorybg = Colors.grey[50];
                        oldbg = Colors.grey[50];
                        idlebg = Colors.grey[50];
                        endbg = Colors.grey[50];
                        col = Colors.red;
                        filteredemp = bfilteredemp;
                      });
                    }
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: ActionChip(
                    avatar: CircleAvatar(
                      backgroundColor: Colors.green,
                      child: Text("$live"),
                    ),
                    label: Text("Live", style: TextStyle(fontSize: 22),),
                    backgroundColor: livebg,
                    shape: BeveledRectangleBorder(),
                    onPressed: () {
                      bfilteredemp =newl.where((element) => 
                      element.stalive == "Live "
                      ).toList();
                      // for(int i=0; i<live; i++){
                      //   for(int j=0; j<emp.length; j++){
                      //     if(emp[j].empId == liveEmp[i].empId){
                      //       bfilteredemp.add(emp[j]);
                      //     }
                      //   }
                      // }
                      setState(() {
                        totalbg = Colors.grey[50];
                        startbg = Colors.grey[50];
                        livebg = Colors.yellow;
                        offlinebg = Colors.grey[50];
                        gpsbg = Colors.grey[50];
                        territorybg = Colors.grey[50];
                        oldbg = Colors.grey[50];
                        idlebg = Colors.grey[50];
                        endbg = Colors.grey[50];
                        col = Colors.green;
                        filteredemp = bfilteredemp;
                      });
                    },
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: ActionChip(
                    avatar: CircleAvatar(
                      backgroundColor: Colors.red[300],
                      child: Text("$off"),
                    ),
                    label: Text("Offline", style: TextStyle(fontSize: 22),),
                    backgroundColor: offlinebg,
                    shape: BeveledRectangleBorder(),
                    onPressed: () {
                      bfilteredemp =newl.where((element) => 
                      element.staoff == "Offline "
                      ).toList();
                      // for(int i=0; i<off; i++){
                      //   for(int j=0; j<emp.length; j++){
                      //     if(emp[j].empId == offlineEmp[i].empId){
                      //       bfilteredemp.add(emp[j]);
                      //     }
                      //   }
                      // }
                      setState(() {
                        totalbg = Colors.grey[50];
                        startbg = Colors.grey[50];
                        livebg = Colors.grey[50];
                        offlinebg = Colors.yellow;
                        gpsbg = Colors.grey[50];
                        territorybg = Colors.grey[50];
                        oldbg = Colors.grey[50];
                        idlebg = Colors.grey[50];
                        endbg = Colors.grey[50];
                        col = Colors.red[300];
                        filteredemp = bfilteredemp;
                      });
                    }
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: ActionChip(
                    avatar: CircleAvatar(
                      backgroundColor: Colors.orange,
                      child: Text("$gps"),
                    ),
                    label: Text("GPS", style: TextStyle(fontSize: 22),),
                    backgroundColor: gpsbg,
                    shape: BeveledRectangleBorder(),
                    onPressed: () {
                      bfilteredemp =newl.where((element) => 
                      element.stagpsoff == "GPS-Offline "
                      ).toList();
                      // for(int i=0; i<gps; i++){
                      //   for(int j=0; j<emp.length; j++){
                      //     if(emp[j].empId == gpsOffEmp[i].empId){
                      //       bfilteredemp.add(emp[j]);
                      //     }
                      //   }
                      // }
                      setState(() {
                        totalbg = Colors.grey[50];
                        startbg = Colors.grey[50];
                        livebg = Colors.grey[50];
                        offlinebg = Colors.grey[50];
                        gpsbg = Colors.yellow;
                        territorybg = Colors.grey[50];
                        oldbg = Colors.grey[50];
                        idlebg = Colors.grey[50];
                        endbg = Colors.grey[50];
                        col = Colors.orange;
                        filteredemp = bfilteredemp;
                      });
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: ActionChip(
                    avatar: CircleAvatar(
                      backgroundColor: Colors.cyan,
                      child: Text("$territory"),
                    ),
                    label: Text("Territory", style: TextStyle(fontSize: 22),),
                    backgroundColor: territorybg,
                    shape: BeveledRectangleBorder(),
                    onPressed: (){
                      bfilteredemp =newl.where((element) => 
                      element.staterritory == "Territory "
                      ).toList();
                      // for(int i=0; i<territory; i++){
                      //   for(int j=0; j<emp.length; j++){
                      //     if(emp[j].empId == territoryEmp[i].empId){
                      //       bfilteredemp.add(emp[j]);
                      //     }
                      //   }
                      // }
                      setState(() {
                        totalbg = Colors.grey[50];
                        startbg = Colors.grey[50];
                        livebg = Colors.grey[50];
                        offlinebg = Colors.grey[50];
                        gpsbg = Colors.grey[50];
                        territorybg = Colors.yellow;
                        oldbg = Colors.grey[50];
                        idlebg = Colors.grey[50];
                        endbg = Colors.grey[50];
                        col = Colors.cyan;
                        filteredemp = bfilteredemp;
                      });
                    }
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: ActionChip(
                    avatar: CircleAvatar(
                      backgroundColor: Colors.green[900],
                      child: Text("$old"),
                    ),
                    label: Text("Old-Start", style: TextStyle(fontSize: 22),),
                    backgroundColor: oldbg,
                    shape: BeveledRectangleBorder(),
                    onPressed: (){
                      bfilteredemp = newl.where((element) => 
                      element.staold == "Old-Start "
                      ).toList();
                      // for(int i=0; i<old; i++){
                      //   for(int j=0; j<emp.length; j++){
                      //     if(emp[j].empId == oldStartEmp[i].empId){
                      //       bfilteredemp.add(emp[j]);
                      //     }
                      //   }
                      // }
                      setState(() {
                        totalbg = Colors.grey[50];
                        startbg = Colors.grey[50];
                        livebg = Colors.grey[50];
                        offlinebg = Colors.grey[50];
                        gpsbg = Colors.grey[50];
                        territorybg = Colors.grey[50];
                        oldbg = Colors.yellow;
                        idlebg = Colors.grey[50];
                        endbg = Colors.grey[50];
                        col = Colors.green[900];
                        filteredemp = bfilteredemp;
                      });
                    }
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: ActionChip(
                    avatar: CircleAvatar(
                      backgroundColor: Colors.pink,
                      child: Text("$idle"),
                    ),
                    label: Text("Idle", style: TextStyle(fontSize: 22),),
                    backgroundColor: idlebg,
                    shape: BeveledRectangleBorder(),
                    onPressed: (){
                      bfilteredemp = newl.where((element) => 
                      element.staidle == "Idle "
                      ).toList();
                      // for(int i=0; i<idle; i++){
                      //   for(int j=0; j<emp.length; j++){
                      //     if(emp[j].empId == idleEmp[i].empId){
                      //       bfilteredemp.add(emp[j]);
                      //     }
                      //   }
                      // }
                      setState(() {
                        totalbg = Colors.grey[50];
                        startbg = Colors.grey[50];
                        livebg = Colors.grey[50];
                        offlinebg = Colors.grey[50];
                        gpsbg = Colors.grey[50];
                        territorybg = Colors.grey[50];
                        oldbg = Colors.grey[50];
                        idlebg = Colors.yellow;
                        endbg = Colors.grey[50];
                        col = Colors.pink;
                        filteredemp = bfilteredemp;
                      });
                    }
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: ActionChip(
                    avatar: CircleAvatar(
                      backgroundColor: Colors.grey,
                      child: Text("$end"),
                    ),
                    label: Text("End", style: TextStyle(fontSize: 22),),
                    backgroundColor: endbg,
                    shape: BeveledRectangleBorder(),
                    onPressed: (){
                      bfilteredemp = newl.where((element) => 
                      element.staend == "End"
                      ).toList();
                      // for(int i=0; i<end; i++){
                      //   for(int j=0; j<emp.length; j++){
                      //     if(emp[j].empId == endEmp[i].empId){
                      //       bfilteredemp.add(emp[j]);
                      //     }
                      //   }
                      // }
                      setState(() {
                        totalbg = Colors.grey[50];
                        startbg = Colors.grey[50];
                        livebg = Colors.grey[50];
                        offlinebg = Colors.grey[50];
                        gpsbg = Colors.grey[50];
                        territorybg = Colors.grey[50];
                        oldbg = Colors.grey[50];
                        idlebg = Colors.grey[50];
                        endbg = Colors.yellow;
                        col = Colors.grey;
                        filteredemp = bfilteredemp;
                      });
                    }
                  ),
                ),
              ],
            ),
          ]),
        ),

        SliverFixedExtentList(
          itemExtent: 50.0,
          delegate: SliverChildListDelegate([
            Table(
              //border: TableBorder.all(),
              columnWidths: {0: FractionColumnWidth(.7), 1: FractionColumnWidth(.3),},
              children: [
                TableRow(
                  children: [
                    TextField(
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(top: 15.0),
                        hintText: "Search...",
                        prefixIcon: Padding(
                          padding: const EdgeInsetsDirectional.only(start: 5.0),
                          child: Icon(Icons.search, color: Colors.black,),
                        ),
                        
                        //icon: Icon(Icons.search, color: Colors.black,),
                      ),
                      onChanged: (string){
                        setState(() {
                          filteredemp = bfilteredemp.where((e) =>
                            e.name.toLowerCase().contains(string.toLowerCase()) ||
                            e.id.toLowerCase().contains(string.toLowerCase())
                          ).toList();
                        });
                      },
                    ),
                    Builder(
                      builder: (context) => new GFButton(
                        text: _date,
                        textStyle: TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.bold),
                        hoverColor: Colors.transparent,
                        type: GFButtonType.transparent,
                        onPressed: () =>  showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(2018),
                          lastDate: DateTime(2030),
                          builder: (BuildContext context, Widget child) {
                            return Theme(
                              data: ThemeData.light(),
                              child: child,
                            );
                          },
                        ).then((date){
                          setState(() {
                            _date = new DateFormat("dd-MMMM-yy").format(date);
                          });
                        })
                      ),
                    ),
                  ],
                ),  
              ],
            ),
            
          ]),
        ),

        SliverList(
          delegate: SliverChildBuilderDelegate(
            (context, index) {
              return Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ListTile(
                      leading: GFIconButton(
                        onPressed: (){},
                        icon: Icon(FontAwesome.map_marker, size: 25.0,),
                        shape: GFIconButtonShape.standard,
                        color: Colors.blue,
                        size: 70.0,
                      ),
                      title: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("${filteredemp[index].name}", style: TextStyle(fontSize: 18.0),),
                          Text("${filteredemp[index].contactNo} (${filteredemp[index].id})", style: TextStyle(color: Colors.blue, fontSize: 14.0),),
                          Text("${filteredemp[index].managerName} | Level:${filteredemp[index].level}", style: TextStyle(fontSize: 16.0),),
                          //Text("${filteredemp[index].stastart}${filteredemp[index].stalive}${filteredemp[index].staoff}${filteredemp[index].staold}${filteredemp[index].staidle}${filteredemp[index].staend}")
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(text: "${filteredemp[index].stastart}", style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold)),
                                TextSpan(text: "${filteredemp[index].stalive}", style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold)),
                                TextSpan(text: "${filteredemp[index].staoff}", style: TextStyle(color: Colors.red[300], fontWeight: FontWeight.bold)),
                                TextSpan(text: "${filteredemp[index].stagpsoff}", style: TextStyle(color: Colors.orange, fontWeight: FontWeight.bold)),
                                TextSpan(text: "${filteredemp[index].staterritory}", style: TextStyle(color: Colors.cyan, fontWeight: FontWeight.bold)),
                                TextSpan(text: "${filteredemp[index].staold}", style: TextStyle(color: Colors.green[900], fontWeight: FontWeight.bold)),
                                TextSpan(text: "${filteredemp[index].staidle}", style: TextStyle(color: Colors.pink, fontWeight: FontWeight.bold)),
                                TextSpan(text: "${filteredemp[index].staend}", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold)),
                              ]
                            )
                          ),
                        ],
                      ),
                      trailing: InkWell(
                          child: Icon(FontAwesome.chevron_circle_right, color: Colors.blue,),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => DeviceStatus(id: filteredemp[index].id,)));
                          }
                      ),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => EmpDetails(id: filteredemp[index].id, name: filteredemp[index].name, mobile: filteredemp[index].contactNo,date: _date,)));
                      },
                    ),
                  ],
                ),
              );
            },
            childCount: filteredemp.length
          ),
        ), 
      ],
    );
  }
}


class Elist{
  String id;
  String name;
  String contactNo;
  String managerName;
  int level;
  String stastart;
  String stalive;
  String staoff;
  String stagpsoff;
  String staterritory;
  String staold;
  String staidle;
  String staend;

  Elist(
    {
      this.id,
      this.name,
      this.contactNo,
      this.managerName,
      this.level,
      this.stastart,
      this.stalive,
      this.staoff,
      this.stagpsoff,
      this.staterritory,
      this.staold,
      this.staidle,
      this.staend
    }
  );
}