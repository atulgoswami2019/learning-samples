import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
//import 'package:mozility/json/Details.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:intl/intl.dart';
//import 'package:mozility/json/DrivRoute.dart';
import 'dart:async';
import 'package:google_maps_flutter/google_maps_flutter.dart';
//import 'package:getflutter/getflutter.dart';
import 'package:sweet_alert_dialogs/sweet_alert_dialogs.dart';



class EmpDetails extends StatefulWidget {
  final String id;
  final String name;
  final String mobile;
  final String date;
  EmpDetails({Key key, this.id, this.name, this.mobile, this.date}) : super(key: key);
  @override
  _EmpDetailsState createState() => _EmpDetailsState();
}

class _EmpDetailsState extends State<EmpDetails> {


  List<LatLng> latlngSegment1 = List();
  LatLng _lastMapPosition = LatLng(28.57442, 77.39246);

  int con = 0;
  double distanse;
  String sTime;
  String eTime;
  Map latLngs;
  List data;

  Future<void> mapLoc(String date, String id) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    print(sharedPreferences.getString('r_token'));
    String token = sharedPreferences.getString('token');
    final response = await http.get('https://ap1.mozility.com/api/common/DriverTrackingDateWise?Id=$id&fromDate=$date&toDate=$date',
    headers: {'Content-Type' : 'application/json', 'Authorization' : 'Bearer $token'}
    );

    final response1 = await http.get('https://ap1.mozility.com/api/common/DriverRouteMap?Id=$id&fromDate=$date&toDate=$date',
    headers: {'Content-Type' : 'application/json', 'Authorization' : 'Bearer $token'}
    );

    if(response.statusCode == 200){
      setState(() {
        data = json.decode(response.body);
      });
      con = data.length > 0 ? 1 : 0;
      for(var i=0; i<data.length; i++){
        //debugPrint("${data[i]["latitude"]}");
        latlngSegment1.add(LatLng(data[i]["lat"], data[i]["long"]));
        if(i==data.length-1){
          setState(() {
            _lastMapPosition = LatLng(data[i]["lat"], data[i]["long"]);
          });
        }
      }
      if(con == 0){
        showDialog(
          context: context,
          builder: (context) => new RichAlertDialog(
            alertTitle: Text("No Location", style: TextStyle(fontSize: 16.0, color: Colors.blue[400]),),
            alertSubtitle: Text("Employee location is not available", style: TextStyle(fontSize: 20.0, color: Colors.green[400], fontWeight: FontWeight.w600),),
            alertType: RichAlertType.ERROR,
            dialogIcon: Icon(FontAwesome.map_marker, size: 16.0, color: Colors.blue,),
            actions: <Widget>[
              FlatButton(
                color: Colors.blue[600],
                textColor: Colors.white,
                child: Text("Close"),
                onPressed: (){Navigator.pop(context);},
              ),
            ],
          ),
        );
      }
    }
    else{
      throw Exception('Failed to load API');
    }

    if(response1.statusCode == 200){
      latLngs = json.decode(response1.body);
      distanse = latLngs["googleDistance"];
      sTime = latLngs["startTime"] == null ? "" : latLngs["startTime"].substring(0, 5);
      eTime = latLngs["endTime"] == null ? "" : latLngs["endTime"].substring(0, 5);
    }
    else{
      throw Exception('Failed to load API');
    }
  }
  
  
  
  @override
  void initState() {
    super.initState();
    mapLoc(widget.date, widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Details"),
        backgroundColor: Colors.blue,
      ),
      backgroundColor: Colors.white70,
      body: Column(
        children: <Widget>[
          Card(
            color: Colors.white70,
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      child: ListTile(
                        leading: Icon(Icons.person, color: Colors.redAccent,),
                        title:  Text("${widget.name}", style: TextStyle(fontSize: 12.0),),
                      )
                     
                    ),
                    Expanded(
                      child: ListTile(
                        leading: Icon(Icons.phone, color: Colors.redAccent,),
                        title:  Text("${widget.mobile}", style: TextStyle(fontSize: 12.0),),
                      )
                    ),     
                  ]
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 6.0, bottom: 4.0),
                  child: Row(
                    children: <Widget>[
                      RichText(
                        text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(text: "Distance: ", style: TextStyle(color: Colors.black)),
                            TextSpan(text: distanse == null ? " " : "$distanse   ", style: TextStyle(color: Colors.blue)),
                            TextSpan(text: "Start Time: ", style: TextStyle(color: Colors.black)),
                            TextSpan(text: sTime == null ? " " : "$sTime   ", style: TextStyle(color: Colors.blue)),
                            TextSpan(text: "End Time: ", style: TextStyle(color: Colors.black)),
                            TextSpan(text: eTime == null ? " " : "$eTime", style: TextStyle(color: Colors.blue))
                          ]
                        )
                      )
                    ]
                  ),
                ),
              ],
            ),
          ),
          
          Row(
            children: <Widget>[
              Expanded(
                child: SizedBox(
                  height: 455.0,
                  child: con == 1 ? GoogleMap(
                    myLocationEnabled: true,
                    initialCameraPosition: CameraPosition(
                      target: _lastMapPosition,
                      zoom: 12.99,
                    ),
                    polylines: Set<Polyline>.of(<Polyline>[
                      Polyline(
                        polylineId: PolylineId('line1'),
                        startCap: Cap.customCapFromBitmap(BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen), refWidth: 5.0),
                        jointType: JointType.round,
                        endCap:Cap.customCapFromBitmap(BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet), refWidth: 5.0),
                        visible: true,
                        points: latlngSegment1,
                        width: 2,
                        color: Colors.blue,
                      )
                    ]),
                  ) : Text("   ")//RichAlertDialog(
                  //   alertTitle: Text("No Location", style: TextStyle(fontSize: 16.0, color: Colors.blue[400]),),
                  //   alertSubtitle: Text("Employee location is not available", style: TextStyle(fontSize: 20.0, color: Colors.green[400], fontWeight: FontWeight.w600),),
                  //   alertType: RichAlertType.ERROR,
                  //   dialogIcon: Icon(FontAwesome.map_marker, size: 16.0, color: Colors.blue,),
                  //   actions: <Widget>[
                  //     FlatButton(
                  //       color: Colors.blue[600],
                  //       textColor: Colors.white,
                  //       child: Text("Close"),
                  //       onPressed: (){Navigator.pop(context);},
                  //     ),
                  //   ],
                  // ),
                )
              )
            ],
          ),
        ] 
      ),
    );
  }
}
