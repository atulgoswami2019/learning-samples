import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mozility/json/Details.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

Future<List<Details>> device(String id) async{
  DateTime _date = DateTime.now();
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  print(sharedPreferences.getString('r_token'));
  String token = sharedPreferences.getString('token');
  final response = await http.get('https://ap1.mozility.com/api/common/DeviceStatus?Id=$id&toDate=$_date',
    headers: <String, String>{'Authorization' : 'Bearer $token'}
  );
  if (response.statusCode == 200) {
    return detailsFromJson(response.body);
  } else {
    throw Exception('Failed to load album');
  }
}

class DeviceStatus extends StatefulWidget {
  final id;
  DeviceStatus({Key key, this.id}) : super(key: key);
  @override
  _DeviceStatusState createState() => _DeviceStatusState();
}

class _DeviceStatusState extends State<DeviceStatus> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Device Status"),
      ),
      body: Container(
        child: FutureBuilder(
          future: device(widget.id),
          builder: (context, snapshot){
            if(snapshot.hasData){
              if(snapshot.data.length > 0){
                String date = snapshot.data[0].timeStamp;
                String _dat = date.substring(0, 11);
                return Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: ListTile(
                            leading: Icon(Icons.phone,),
                            title:  Text("${snapshot.data[0].contactNo}", style: TextStyle(fontSize: 12.0),),
                          )
                        
                        ),
                        Expanded(
                          child: ListTile(
                            leading: Icon(Icons.calendar_today,),
                            title:  Text("$_dat", style: TextStyle(fontSize: 12.0),),
                          )
                        ),     
                      ]
                    ),

                    Table(
                      border: TableBorder.all(),
                      children: [
                        TableRow(
                          children: [
                            Container(child: Center(child: Text("Data")), height: 50, color: Colors.grey[400],),
                            Container(child: Center(child: Text("${snapshot.data[0].dataStatus}")), height: 50, color: snapshot.data[0].dataStatus == "ON" ? Colors.green[400] : Colors.red[800],),
                          ]
                        ),
                        TableRow(
                          children: [
                            Container(child: Center(child: Text("GPS")), height: 50, color: Colors.grey[400],),
                            Container(child: Center(child: Text("${snapshot.data[0].gpsStatus}")), height: 50, color: snapshot.data[0].gpsStatus == "ON" ? Colors.green[400] : Colors.red[800],),
                          ]
                        ),
                        TableRow(
                          children: [
                            Container(child: Center(child: Text("Mode")), height: 50, color: Colors.grey[400],),
                            Container(child: Center(child: Text("${snapshot.data[0].gpsMode}")), height: 50, color: snapshot.data[0].gpsMode == "HIGH ACCURACY" ? Colors.green[400] : Colors.red[800],),
                          ]
                        ),
                        TableRow(
                          children: [
                            Container(child: Center(child: Text("Battery")), height: 50, color: Colors.grey[400],),
                            Container(child: Center(child: Text("${snapshot.data[0].batteryPer}")), height: 50, color: snapshot.data[0].batteryPer > 75 ? Colors.green[400] : snapshot.data[0].batteryPer > 50 ? Colors.green[300] : snapshot.data[0].batteryPer > 30 ? Colors.lightBlueAccent : Colors.red[800],),
                          ]
                        ),
                        TableRow(
                          children: [
                            Container(child: Center(child: Text("Charging")), height: 50, color: Colors.grey[400],),
                            Container(child: Center(child: Text("${snapshot.data[0].chargingStatus}")), height: 50, color: snapshot.data[0].chargingStatus == "ON" ? Colors.green[400] : Colors.red[800],),
                          ]
                        ),
                      ],
                    ),
                    
                  ],
                );
              } else {
                return Center(child: Text("Not Available"));
              }
            } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }

              return Center(child: CupertinoActivityIndicator(radius: 40.0,));
          }
        ),
      ),
    );
  }
}