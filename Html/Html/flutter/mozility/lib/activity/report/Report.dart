import 'package:flutter/material.dart';

class Report extends StatefulWidget {
  @override
  _ReportState createState() => _ReportState();
}

class _ReportState extends State<Report> {

  var currentItemSelected = 'Select';
  String _value;
  String two = "Ietm 2";

  var teams = [
    'Select',
    'W1 | Admin',
    'WP-001 | Atul | 9891807998',
    'MOZ001 | Mozility Manager | 29891807998',
    'SC001 | Tarun Bhargava | 9810117434',
    'XO3D-02 | XO3D Manager | 447590551811',
    'DEMO001 | Vijay | 8802220090',
    'MOZ | Dummy | 9876543210',
    'SE027 | Binay Bansal | 7009550827',
    'SE001 | Vishal Singh | 8851136757',
    'VIJ001 | Vijay Demo | 8700866263',
    'SV03 | Vijendra | 9711612454',
    'DRN001 | Prashant Bhardwaj | 9873663979',
    'GAS001 | Anubhav Hazra | 9850885056',
    'SC002 | Dewaker Joshi | 8826297558',
    'SC003 | Devendra Kumar | 9582308540',
    'SC004 | Maya Bhoj | 9811440221',
    'SC005 | Pranav Dubey | 9650719882',
    'MO20 | Anamika Jindal | 7011064032',
    'RT01 | Rajesh Tiwari | 8858600090',
    'DEMO_BR | Jitesh Lalwani | 9829672677',
    'AB001 | Shivam Dixit | 9765390786',
    'NTC01 | Yogesh | 9810510280',
    'NTC02 | Sunil | 9212666120',
    'VG001 | Vedpal Godara_Demo | 7300088825',
    'AP001 | Ashish Panwar_Demo | 7665437071',
    'JL001 | Jitesh Lalwani | 19829672677',
    'GAS002 | Sagar Deore | 9123018915',
    'GAS003 | Dinesh Dhobale | 9766575466',
    'GA002 | Vatsal Naik | 8796607550',
    'GA003 | Pankaj Watpade | 9405926787',
    'AYU001 | Ayush Jawahar | 96438320201',
    'SE002 | Rakesh | 7676632210',
    'SE003 | Amrendra Shrivastava | 3231908872',
    'SE004 | Rajan | 6756565448',
    'SE005 | Biresh Chandra | 6543432322',
    'SE006 | Chandan | 7678542298'
  ];
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Reports"),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            DropdownButton<String>(
              isExpanded: true,
              value: currentItemSelected,
              items: teams.map((String dropdownItems) => 
                DropdownMenuItem<String>(
                  value: dropdownItems,
                  child: Text(dropdownItems),
                ),
              ).toList(),
              onChanged: (String selected){
                setState(() {
                  currentItemSelected = selected;
                });
              }
            ),

            DropdownButton<String>(
          items: [
            DropdownMenuItem<String>(
              child: Row(
                  children: <Widget>[
                    Icon(Icons.filter_1),
                    Text(''),
                  ],
              ),
              value: 'one',
            ),
            DropdownMenuItem<String>(
              child: Row(
                children: <Widget>[
                  Icon(Icons.filter_2),
                  Text('Item 2'),
                ],
              ),
              value: 'two',
              onTap: (){
                setState(() {
                  two = "123";
                });
              },
            ),
            DropdownMenuItem<String>(
              child: Row(
                children: <Widget>[
                  Icon(Icons.filter_3),
                  Text('Item 3'),
                ],
              ),
              value: 'three3',
            ),
          ],
          isExpanded: false,
          onChanged: (String value) {
            setState(() {
              _value = value;
            });
          },
          hint: Text('Select Item'),
          value: _value,
          underline: Container(
            decoration: const BoxDecoration(
                border: Border(bottom: BorderSide(color: Colors.grey))
            ),
          ),
          style: TextStyle(
            fontSize: 30,
            color: Colors.black,
          ),
          iconEnabledColor: Colors.pink,
          iconSize: 40,
        ),
          ],
        ),
      ),
    );
  }
}