import 'package:flutter/material.dart';
import 'package:getflutter/getflutter.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:mozility/json/Employee.dart';
import 'package:mozility/activity/emp_Details.dart';
import 'package:mozility/activity/DeviceStates.dart';

class EmpList extends StatefulWidget {
  final List<Employee> elist;
  final Color col;
  EmpList({Key key, this.elist, this.col}) : super(key: key);
  @override
  _EmpListState createState() => _EmpListState();
}

class _EmpListState extends State<EmpList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("List"),
      ),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverList(
          delegate: SliverChildBuilderDelegate(
            (context, index) {
              return Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ListTile(
                      leading: GFIconButton(
                        onPressed: (){},
                        icon: Icon(FontAwesome.map_marker, size: 25.0,),
                        shape: GFIconButtonShape.standard,
                        color: widget.col,
                        size: 70.0,
                      ),
                      trailing: InkWell(
                          child: Icon(FontAwesome.chevron_circle_right, color: Colors.blue,),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => DeviceStatus(id: widget.elist[index].empId,)));
                          }
                      ),
                      title: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("${widget.elist[index].empName}", style: TextStyle(fontSize: 18.0),),
                          Text("${widget.elist[index].empContactNo} (${widget.elist[index].empId})", style: TextStyle(color: Colors.blue, fontSize: 14.0),),
                          Text("${widget.elist[index].managerName} | Level:${widget.elist[index].empLevel}", style: TextStyle(fontSize: 16.0),)
                        ],
                      ),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => EmpDetails(id: widget.elist[index].empId, name: widget.elist[index].empName, mobile: widget.elist[index].empContactNo,)));
                      },
                    ),
                  ],
                ),
              );
            },
            childCount: widget.elist.length
          ),
        ), 
        ],
      ),
    );
  }
}