import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:getflutter/getflutter.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:intl/intl.dart';
import 'package:mozility/activity/map_track.dart';
import 'package:mozility/activity/report/Report.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mozility/main.dart';
import 'package:mozility/token/refresh_token.dart';



class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  String fname;
  String lname;
  String email;
  String expire;
  DateTime d;
  DateTime now;
  String c;

  SharedPreferences sharedPreferences;

  Timer timer;

  @override
  void initState() {
    super.initState();
    checkLoginStatus();
    timer = Timer.periodic(Duration(minutes: 50), (Timer t) => Token.refreshToken());
  }

  @override
void dispose() {
  timer?.cancel();
  super.dispose();
}


  checkLoginStatus() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    expire = sharedPreferences.getString('expire');
    if(expire == null){
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => LoginPage()),
          (Route<dynamic> route) => false);
    }
    else{
      d = DateFormat("EE, dd MMMM yyyy HH:mm:ss 'GMT'").parse(expire);
      d = d.add(Duration(hours: 5, minutes: 30));
      now = DateTime.now();
      if(d.difference(now).inMinutes < 0){
        Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => LoginPage()),
            (Route<dynamic> route) => false);
      }
      else{
        setState(() {
          fname = sharedPreferences.getString('fname');
          lname = sharedPreferences.getString('lname');
          email = sharedPreferences.getString('email');
          c = fname.substring(0, 1).toUpperCase();
        });
      }
    }
    
  }

  final List<String> imageList = [
  'images/I1.jpg',
  'images/I2.jpg',
  'images/I3.jpg',
];

  @override
  Widget build(BuildContext context) {
    Widget imageCarousel = GFCarousel(
      height: 170,
      autoPlayCurve: Curves.decelerate,
      activeIndicator: Colors.black,
      autoPlay: true,
      items: imageList.map(
        (url) {
          return ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
              child: Container(
                margin: EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(url),
                  fit: BoxFit.cover
                )
              ), 
            ),
          );
        },
      ).toList(),
    );
    return Scaffold(
      appBar: AppBar(
        title: Text("Mozility"),
      ),
      
       drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text("$fname $lname"),
              accountEmail: Text("$email"),
              currentAccountPicture: CircleAvatar(
                backgroundColor: Colors.cyanAccent,
                child: Text("$c", style: TextStyle(fontSize: 30.0,),),
              ),
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            ),
            ListTile(
              title: Text('Inbox'),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).pushNamed("/inbox");
                
            
              },
              ),
            ListTile(
              title: Text('Change Password'),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).pushNamed("/sent");
              },
            ),

            ListTile(
                title: Text("Log out"),
                trailing: Icon(Icons.close),
                onTap: () async{
                  //sharedPreferences.remove('token');
                  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
                        await sharedPreferences.clear();

                   Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (context) => LoginPage()),
                      (Route<dynamic> route) => false);
                },
            ),
          ],
        ),
      ),
      body: new ListView(
        children: <Widget>[
          imageCarousel,
         

          Card(
           
            child: GridView.count(
              crossAxisCount: 3,
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              children: <Widget>[
                ListTile(
                  title: Icon(FontAwesome.dashboard, color: Colors.blue,),
                  subtitle: Text("Dasboard", textAlign: TextAlign.center,),
                ),
                ListTile(
                  title: Icon(FontAwesome.map_marker, color: Colors.blue,),
                  subtitle: Text("Map Tracker", textAlign: TextAlign.center,),
                  onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MapTrack())
                    );
                  },
                ),
                ListTile(
                  title: Icon(FontAwesome.sitemap, color: Colors.blue,),
                  subtitle: Text("HRM", textAlign: TextAlign.center,),
                ),
                ListTile(
                  title: Icon(FontAwesome.line_chart, color: Colors.blue,),
                  subtitle: Text("CRM", textAlign: TextAlign.center,),
                ),
                ListTile(
                  title: Icon(FontAwesome.file, color: Colors.blue,),
                  subtitle: Text("Report", textAlign: TextAlign.center,),
                  onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Report())
                    );
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

