import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

 
class Token{
  static refreshToken() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map body = {
      'client_id' : "ngAuthApp",
      'refresh_token' : sharedPreferences.getString('r_token'),
      'grant_type' : "refresh_token",
    };
    var jsonData;
    var response = await http.post("https://ap1.mozility.com/oauth/token",
      headers: <String, String>{ 'Content' : 'application/x-www-form-urlencoded'},
      body: body,
    );
    if(response.statusCode == 200){
      print("success");
      jsonData = json.decode(response.body);
      sharedPreferences.setString('token', jsonData['access_token']);
      sharedPreferences.setString('r_token', jsonData['refresh_token']);
      sharedPreferences.setString('fname', jsonData['firstName']);
      sharedPreferences.setString('lname', jsonData['lastName']);
      sharedPreferences.setString('email', jsonData['emailId']);
      sharedPreferences.setString('adminEID', jsonData['EmpId']);
      sharedPreferences.setString('expire', jsonData['.expires']);  
    }
    else{
      throw Exception("Something wrong!");
    }
  }
}