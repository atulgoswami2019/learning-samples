import 'dart:convert';

List<Location> locationFromJson(String str) => List<Location>.from(json.decode(str).map((x) => Location.fromJson(x)));

String locationToJson(List<Location> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Location {
    String id;
    String empId;
    String name;
    DateTime timeStamp;
    double lat;
    double long;
    String userActivity;
    String contactNo;
    String gpsStatus;
    String dataStatus;
    int interritory;
    String otherInfo;
    String cDate;

    Location({
        this.id,
        this.empId,
        this.name,
        this.timeStamp,
        this.lat,
        this.long,
        this.userActivity,
        this.contactNo,
        this.gpsStatus,
        this.dataStatus,
        this.interritory,
        this.otherInfo,
        this.cDate,
    });

    factory Location.fromJson(Map<String, dynamic> json) => Location(
        id: json["\u0024id"],
        empId: json["empId"],
        name: json["name"],
        timeStamp: DateTime.parse(json["timeStamp"]),
        lat: json["lat"].toDouble(),
        long: json["long"].toDouble(),
        userActivity: json["userActivity"],
        contactNo: json["contactNo"],
        gpsStatus: json["gpsStatus"],
        dataStatus: json["dataStatus"],
        interritory: json["interritory"],
        otherInfo: json["otherInfo"],
        cDate: json["cDate"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "empId": empId,
        "name": name,
        "timeStamp": timeStamp.toIso8601String(),
        "lat": lat,
        "long": long,
        "userActivity": userActivity,
        "contactNo": contactNo,
        "gpsStatus": gpsStatus,
        "dataStatus": dataStatus,
        "interritory": interritory,
        "otherInfo": otherInfo,
        "cDate": cDate,
    };
}