import 'dart:convert';

List<RouteMap> routeMapFromJson(String str) => List<RouteMap>.from(json.decode(str).map((x) => RouteMap.fromJson(x)));

String routeMapToJson(List<RouteMap> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class RouteMap {
    String id;
    double googleDistance;
    String startTime;
    String endTime;
    List<LatLng> latLngs;

    RouteMap({
        this.id,
        this.googleDistance,
        this.startTime,
        this.endTime,
        this.latLngs,
    });

    factory RouteMap.fromJson(Map<String, dynamic> json) => RouteMap(
        id: json["\u0024id"],
        googleDistance: json["googleDistance"].toDouble(),
        startTime: json["startTime"],
        endTime: json["endTime"],
        latLngs: List<LatLng>.from(json["latLngs"].map((x) => LatLng.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "googleDistance": googleDistance,
        "startTime": startTime,
        "endTime": endTime,
        "latLngs": List<dynamic>.from(latLngs.map((x) => x.toJson())),
    };
}

class LatLng {
    String id;
    double latitude;
    double longitude;

    LatLng({
        this.id,
        this.latitude,
        this.longitude,
    });

    factory LatLng.fromJson(Map<String, dynamic> json) => LatLng(
        id: json["\u0024id"],
        latitude: json["latitude"].toDouble(),
        longitude: json["longitude"].toDouble(),
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "latitude": latitude,
        "longitude": longitude,
    };
}