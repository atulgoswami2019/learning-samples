import 'dart:convert';

List<Employee> employeeFromJson(String str) => List<Employee>.from(json.decode(str).map((x) => Employee.fromJson(x)));

String employeeToJson(List<Employee> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Employee {
    String id;
    int rowId;
    String empId;
    String empName;
    String empContactNo;
    String managerid;
    String managerName;
    int roleId;
    String roleName;
    String userType;
    int empLevel;
    String userName;
    String uuid;

    Employee({
        this.id,
        this.rowId,
        this.empId,
        this.empName,
        this.empContactNo,
        this.managerid,
        this.managerName,
        this.roleId,
        this.roleName,
        this.userType,
        this.empLevel,
        this.userName,
        this.uuid,
    });

    factory Employee.fromJson(Map<String, dynamic> json) => Employee(
        id: json["\u0024id"],
        rowId: json["rowId"],
        empId: json["empId"],
        empName: json["empName"],
        empContactNo: json["empContactNo"] == null ? null : json["empContactNo"],
        managerid: json["managerid"] == null ? null : json["managerid"],
        managerName: json["managerName"],
        roleId: json["roleId"],
        roleName: json["roleName"],
        userType: json["userType"],
        empLevel: json["empLevel"],
        userName: json["userName"],
        uuid: json["uuid"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "rowId": rowId,
        "empId": empId,
        "empName": empName,
        "empContactNo": empContactNo == null ? null : empContactNo,
        "managerid": managerid == null ? null : managerid,
        "managerName": managerName,
        "roleId": roleId,
        "roleName": roleName,
        "userType": userType,
        "empLevel": empLevel,
        "userName": userName,
        "uuid": uuid,
    };
}