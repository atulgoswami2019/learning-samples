import 'dart:convert';

List<Details> detailsFromJson(String str) => List<Details>.from(json.decode(str).map((x) => Details.fromJson(x)));

String detailsToJson(List<Details> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Details {
    String id;
    String timeStamp;
    String contactNo;
    String gpsStatus;
    String dataStatus;
    String gpsMode;
    int batteryPer;
    String chargingStatus;
    String otherInfo;
    String appVersion;

    Details({
        this.id,
        this.timeStamp,
        this.contactNo,
        this.gpsStatus,
        this.dataStatus,
        this.gpsMode,
        this.batteryPer,
        this.chargingStatus,
        this.otherInfo,
        this.appVersion,
    });

    factory Details.fromJson(Map<String, dynamic> json) => Details(
        id: json["\u0024id"],
        timeStamp: json["timeStamp"],
        contactNo: json["contactNo"],
        gpsStatus: json["gpsStatus"],
        dataStatus: json["dataStatus"],
        gpsMode: json["gpsMode"],
        batteryPer: json["batteryPer"].toInt(),
        chargingStatus: json["chargingStatus"],
        otherInfo: json["otherInfo"],
        appVersion: json["appVersion"],
    );

    Map<String, dynamic> toJson() => {
        "\u0024id": id,
        "timeStamp": timeStamp,
        "contactNo": contactNo,
        "gpsStatus": gpsStatus,
        "dataStatus": dataStatus,
        "gpsMode": gpsMode,
        "batteryPer": batteryPer,
        "chargingStatus": chargingStatus,
        "otherInfo": otherInfo,
        "appVersion": appVersion,
    };
}