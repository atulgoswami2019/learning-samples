import 'package:http/http.dart' as http;
import 'package:mozility/json/Location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

class Services{
 static Future<List<Location>> drivLocationEmp() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String _date = new DateFormat("dd-MMMM-yy").format(DateTime.now());
    String token = sharedPreferences.getString('token');
    String id = sharedPreferences.getString('adminEID');
    final response = await http.get('https://ap1.mozility.com/api/common/DriversCurrentLocation?Id=$id&fromDate=$_date&toDate=$_date',
    headers: <String, String>{'Authorization' : 'Bearer $token'}
    );
    if(response.statusCode == 200){
      return locationFromJson(response.body);
    }
    else{
      throw Exception('Failed to load API');
    }
    
  }
}