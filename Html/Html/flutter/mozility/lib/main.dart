import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:mozility/Home.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweet_alert_dialogs/sweet_alert_dialogs.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Mozility Manager App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Home(),
    );
  }
}

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  bool _isLoading = false;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(padding: EdgeInsets.all(0.0),
      child:
      Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Colors.blue,
            Colors.teal,
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: _isLoading ? Center(child: CircularProgressIndicator(),) : ListView(
        children: <Widget>[
          headerSection(),
          textSection(),
          buttonSection(),
        ],
      ),
      ),
      ),
    );
  }

  singIn(String name, String password) async{
  Map data = {
    'client_id' : "ngAuthApp",
    'username' : name,
    'password' : password,
    'grant_type' : "password",
  };
  var jsonData;
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var response = await http.post("https://ap1.mozility.com/oauth/token",
    headers: <String, String>{ 'Content' : 'application/x-www-form-urlencoded'},
    body: data,
  );
  
  if(response.statusCode == 200){
    jsonData = json.decode(response.body);
    setState(() {
      _isLoading = false;
      sharedPreferences.setString('token', jsonData['access_token']);
      sharedPreferences.setString('r_token', jsonData['refresh_token']);
      sharedPreferences.setString('fname', jsonData['firstName']);
      sharedPreferences.setString('lname', jsonData['lastName']);
      sharedPreferences.setString('email', jsonData['emailId']);
      sharedPreferences.setString('adminEID', jsonData['EmpId']);
      sharedPreferences.setString('expire', jsonData['.expires']);
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => Home()),
        (Route<dynamic> route) => false);
    });
  }
  else{
    showDialog(
      context: context,
      builder: (context) => new RichAlertDialog(
        alertTitle: Text("Error", style: TextStyle(fontSize: 16.0, color: Colors.blue[400]),),
        alertSubtitle: Text("Username or password is incorrect!", style: TextStyle(fontSize: 18.0, color: Colors.green[400], fontWeight: FontWeight.w600),),
        alertType: RichAlertType.ERROR,
        actions: <Widget>[
          FlatButton(
            color: Colors.blue[600],
            textColor: Colors.white,
            child: Text("Close"),
            onPressed: (){
              Navigator.pop(context);
              setState(() {
                _isLoading = false;
              });
            },
          ),
        ],
      ),
    );   
  }
}

  Container buttonSection(){
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 40.0,
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      margin: EdgeInsets.only(top: 30.0),
      child: RaisedButton(
        onPressed: (){
          setState(() {
            _isLoading = true;
          });
          singIn(nameController.text, passwordController.text);
        },
        color: Colors.purple,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Text("Sing In", style: TextStyle(color: Colors.white70),),
      ),
    );
  }

  Container textSection() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      margin: EdgeInsets.only(top: 30.0),
      child: Column(
        children: <Widget>[
          txtName("User Name", Icons.email),
          SizedBox(height: 30.0,),
          txtPassword("Password", Icons.lock),

        ],
      ),
    );
  }

  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  TextFormField txtName(String title, IconData icon){
    return TextFormField(
      controller: nameController,
      style: TextStyle(color: Colors.white70),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: Colors.white70),
        icon: Icon(icon),
      ),
    );
  }

  TextFormField txtPassword(String title, IconData icon){
    return TextFormField(
      controller: passwordController,
      obscureText: true,
      style: TextStyle(color: Colors.white70),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: Colors.white70),
        icon: Icon(icon),
        focusColor: Colors.red,
      ),
    );
  }

  Container headerSection(){
    return Container(
      padding: EdgeInsets.only(top: 50.0),             
        child: CircleAvatar(
          radius: 50.0,
          child: new Image.asset("images/mozility.png"),
          backgroundColor: Colors.transparent,              
        ),
    );
  }
}