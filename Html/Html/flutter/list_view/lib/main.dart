import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    title: "Settings",
    home: Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.settings),
        title: Text("SETTINGS"),
      ),
      body: getListView(),
    ),
  ));
}

Widget getListView(){
  var listView = ListView(
    children: <Widget>[
      ListTile(
        leading: Icon(Icons.bluetooth),
        title: Text("Bluetooth"),
        subtitle: Text("Connect to bluetooth devices"),
      ),

       ListTile(
        leading: Icon(Icons.notifications),
        title: Text("Notifications"),
        subtitle: Text("Block,allow,prioritize"),
      ),

       ListTile(
        leading: Icon(Icons.wallpaper),
        title: Text("Wallpaper"),
        subtitle: Text("Set Wallpaper"),
      ),

       ListTile(
        leading: Icon(Icons.apps),
        title: Text("Apps"),
        subtitle: Text("Default apps, App permissions"),
      ),
    ],
  );
  return listView;
}
