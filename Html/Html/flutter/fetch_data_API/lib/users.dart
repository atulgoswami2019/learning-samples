import 'dart:convert';

List<Users> usersFromJson(String str) => List<Users>.from(json.decode(str).map((x) => Users.fromJson(x)));

String usersToJson(List<Users> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Users {
    String id;
    String name;
    String email;
    String mobile;
    String password;

    Users({
        this.id,
        this.name,
        this.email,
        this.mobile,
        this.password,
    });

    factory Users.fromJson(Map<String, dynamic> json) => Users(
        id: json["Id"],
        name: json["Name"],
        email: json["Email"],
        mobile: json["Mobile"],
        password: json["Password"],
    );

    Map<String, dynamic> toJson() => {
        "Id": id,
        "Name": name,
        "Email": email,
        "Mobile": mobile,
        "Password": password,
    };
}