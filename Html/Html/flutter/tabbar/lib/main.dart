import 'package:flutter/material.dart';

void main() {
  runApp(Tabbar());
}

class Tabbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              tabs: [
                Tab(text: "Apps"),
                Tab(text: "Movies"),
                Tab(text: "Games"),
              ],
              ),
            title: Text("Tab Demo"),
          ),
          body: TabBarView(
            children:[
              Text("Apps"),
              Text("Movie"),
              Text("Games"),
            ]
          ),
        ),
      ),
    );
  }
}