import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:login_api/main.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Colors.blue,
            Colors.teal,
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),*/
      body: Padding(padding: EdgeInsets.all(10),
      child:
    _isLoading ? Center(child: CircularProgressIndicator(),) : ListView(
  
          children: <Widget>[
  
            headerSection(),
  
            textSection(),
  
            buttonSection(),
  
          ],
  
        ),

      ),
    );
    
  }

  singIn(String name, String password) async{
  Map data = {
    'client_id' : "ngAuthApp",
    'username' : name,
    'password' : password,
    'grant_type' : "password",
  };
  var jsonData = null;
  SharedPreferences sharedPreference = await SharedPreferences.getInstance();
  var response = await http.post("https://ap1.mozility.com/oauth/token",
    headers: <String, String>{ 'Content' : 'application/x-www-form-urlencoded'},
    body: data,
  );
  
  if(response.statusCode == 200){
    jsonData = json.decode(response.body);
    setState(() {
      _isLoading = false;
      sharedPreference.setString("access_token", jsonData['access_token']);
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => MainPage()),
        (Route<dynamic> route) => false);
    });
  }
  else{
    print(response.body);
  }
}

  Container buttonSection(){
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 40.0,
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      margin: EdgeInsets.only(top: 30.0),
      child: RaisedButton(
        onPressed: (){
          setState(() {
            _isLoading = true;
          });
          singIn(nameController.text, passwordController.text);
        },
        color: Colors.purple,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Text("Sing In", style: TextStyle(color: Colors.red),),
      ),
    );
  }

  Container textSection() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      margin: EdgeInsets.only(top: 30.0),
      child: Column(
        children: <Widget>[
          txtName("Email", Icons.email),
          SizedBox(height: 30.0,),
          txtPassword("Password", Icons.lock),
        ],
      ),
    );
  }

  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  TextFormField txtName(String title, IconData icon){
    return TextFormField(
      controller: nameController,
      style: TextStyle(color: Colors.red),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: Colors.red),
        icon: Icon(icon),
      ),
    );
  }

  TextFormField txtPassword(String title, IconData icon){
    return TextFormField(
      controller: passwordController,
      obscureText: true,
      style: TextStyle(color: Colors.red),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: Colors.red),
        icon: Icon(icon),
      ),
    );
  }

  Container headerSection(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 30.0),
      child: Text("Mozility", style: TextStyle(color: Colors.red),),
    );
  }
}
   