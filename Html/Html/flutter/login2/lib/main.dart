

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login',
      theme: ThemeData(
        
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Login"),
        ),
        body: LoginHome(),
      ),
    );
  }
}

class LoginHome extends StatefulWidget {
  @override
  _LoginHomeState createState() => _LoginHomeState();
}

class _LoginHomeState extends State<LoginHome> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController emailController = TextEditingController();
  TextEditingController passController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            controller: emailController,
            decoration: const InputDecoration(
            icon: const Icon(Icons.email),
            hintText: "Enter your Email",
            labelText: "Email",
            
            ),
            validator: (value){
              if(value.isEmpty){
                return 'Please enter your email';
              }
              return null;
            },
          ),
           TextFormField(
             controller: passController,
            decoration: const InputDecoration(
            icon: const Icon(Icons.lock),
            hintText: "Enter your Password",
            labelText: "Password",
            ),
            validator: (value){
              if(value.isEmpty){
                return 'Please enter your Password';
              }
              return null;
            },
          ),
          new Container(
            padding: const EdgeInsets.only(left: 150.0, top: 40.0),
            child: new RaisedButton(
              child: const Text("Submit"),
              onPressed: (){
                  if(_formKey.currentState.validate()){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context)=> Login(email: emailController.text, pass: passController.text))
                    );
                  }
              }
              ),
          ),
        ],
      ),
    );
  }
}

class Login extends StatefulWidget {
  final String email;
  final String pass;
  Login({Key key, this.email, this.pass}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Welcome"),
      ),
      body: Text("${widget.email},${widget.pass}"),
            
    );
  }
}
